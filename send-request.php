<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
  exit;
}

// disabilitiamo gli errori a video
@error_reporting(0);
@ini_set('display_errors', '0');
// gestione degli errori
function fatalHandler(){
  @http_response_code(500);
  exit;
}
//register_shutdown_function("fatalHandler");
// impostiamo la timezone su UTC per avere l'orario allineato con le API di terze parti
@date_default_timezone_set('UTC');
// includiamo il file di configurazione
@require_once("config.php");
// facciamo il trim dei dati ricevuti in POST
$_POST = @array_map("trim", $_POST);

/* API#1/2 - AUTENTICAZIONE */
$ch = @curl_init();
if($ch === false){
  @http_response_code(500);
  exit;
}
@curl_setopt($ch, CURLOPT_URL,            "https://auth.exacttargetapis.com/v1/requestToken" );
@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
@curl_setopt($ch, CURLOPT_POST,           1 );
@curl_setopt($ch, CURLOPT_POSTFIELDS,     '{"clientId": "'.API_CLIENT_ID.'", "clientSecret": "'.API_CLIENT_SECRET.'"}');
//@curl_setopt($ch, CURLOPT_POSTFIELDS,     '{"clientId": "ehotuvvhqo2xru477mxl3vz1", "clientSecret": "93hdFCnQnxSDrDyV4EXz6Oxm"}');
@curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json; charset=utf8'));
if (DEBUG){
  // this options give information about the request string sent
  @curl_setopt($ch, CURLINFO_HEADER_OUT, true);
}

$result = @curl_exec($ch);
$errors = @curl_error($ch);
$http_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
if (DEBUG){
  $info = @curl_getinfo($ch);
}
@curl_close($ch);

if ($http_code >= 300) {
  header('HTTP/1.1 503 Service Temporarily Unavailable');
  if (DEBUG){
    header('Content-Type: application/json');
    echo json_encode($info);
  }
  exit;
}

// aquisiamo i dati della risposta
$data = json_decode($result, true);

$characters = '0123456789';
$charactersLength = strlen($characters);
$randomString = '';
for ($i = 0; $i < 5; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
}

$randomID=RANDOM_PREFIX.$randomString;

$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
$charactersLength = strlen($characters);
$randomString = '';
for ($i = 0; $i < 5; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
}

$randomID .= "-".$randomString."-".date("hms");

// formattazione del payload per la seconda chiamata
//$agreenewsletter = intval($_POST["agreenewsletter"]);
$agreeresearch = intval($_POST["agreeMarketResearch"]);
$agreedata = intval($_POST["agreeData"]);
$agreeprofiling = intval($_POST["agreeMarketProfiling"]);
$agreesurvey = intval($_POST["agreeSurvey"]);



$payload = '
{
  "To": {
    "Address": "'.$_POST["email"].'",
    "SubscriberKey": "'.$_POST["email"].'",
    "ContactAttributes": {
      "SubscriberAttributes": {
        "EmailAddress": "'.$_POST["email"].'",
		"initialData_tipo": "liberi professionisti",
        "initialData_firstname": "'.$_POST["forename"].'",
        "initialData_lastname": "'.$_POST["surname"].'",
		"initialData_CAP": "'.$_POST["postcode"].'",
		"initialData_mobile": "'.$_POST["mobile"].'",
		"initialData_carmodel": "'.$_POST["car_model"].'",
		"initialData_tipoacquisto": "'.$_POST["tipo_acquisto"].'",
        "initialData_consent_marketResearch": '.$agreeresearch.',
        "initialData_consent_data": '.$agreedata.',
        "initialData_consent_marketProfiling": '.$agreeprofiling.',
        "initialData_consent_survey": '.$agreesurvey.'
      }
    }
  },
  "OPTIONS": {
    "RequestType": "ASYNC"
  }
}'; 







$ch = @curl_init();
if($ch === false){
  @http_response_code(500);
  exit;
}
@curl_setopt($ch, CURLOPT_URL,            "https://www.exacttargetapis.com/messaging/v1/messageDefinitionSends/key:".API_TRIGGERED_SEND."/send" );
@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
@curl_setopt($ch, CURLOPT_POST,           true );
@curl_setopt($ch, CURLOPT_POSTFIELDS,     $payload );
@curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json', 'Authorization: Bearer '.trim($data["accessToken"])));
if (DEBUG){
  // this options give information about the request string sent
  @curl_setopt($ch, CURLINFO_HEADER_OUT, true);
}

$result = @curl_exec($ch);
$errors = @curl_error($ch);
$http_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
if (DEBUG){
  $info = @curl_getinfo($ch);
}
@curl_close($ch);

if ($http_code >= 300) {
  header('HTTP/1.1 503 Service Temporarily Unavailable');
  if (DEBUG){
    header('Content-Type: application/json');
    echo json_encode($info);
  }
  exit;
}

/* RICHIESTA TERMINATA CON SUCCESSO RESTITUIAMO IL JSON DI RISPOSTA */
header('Content-Type: application/json');
echo json_encode($result);
?>
