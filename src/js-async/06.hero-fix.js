var heroFix = function(){
  console.log('heroFix fired');
  var container = $('.hero-background');
  var imgUrl = $('.hero-background .background').prop('src');
  if (imgUrl) {
    container.css('backgroundImage', 'url(' + imgUrl + ')').addClass('compat-object-fit');
  }
/*
  $('.hero-background').each(function () {
    var $container = $(this),
        imgUrl = $container.find('img:visible').prop('src');
    console.log('container');
    console.log($container);
    console.log('imgUrl');
    console.log(imgUrl);
    if (imgUrl) {
      $container
        .css('backgroundImage', 'url(' + imgUrl + ')')
        .addClass('compat-object-fit');
    }
  });
*/
};

$(function(){
  if ( ! Modernizr.objectfit ) {
    // correzione hero per IE
    heroFix();
    $( window ).resize(function() {
      heroFix();
    });
  }
});
