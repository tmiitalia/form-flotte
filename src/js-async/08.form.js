function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validatePhone(phone) {
  var re = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
  return re.test(phone) && phone.length >= 8;
}

function validate() {
  var valid = true;

  // validazione del nome
  if ($('#forename').val() == ""){
    $('#forename').blur().addClass('is-invalid');
    valid = false;
  }
  
    // validazione del indirizzo
  if ($('#address').val() == ""){
    $('#address').blur().addClass('is-invalid');
    valid = false;
  }
  
    // validazione del citta
  if ($('#citta').val() == ""){
    $('#citta').blur().addClass('is-invalid');
    valid = false;
  }
  
    // validazione del nome
  if ($('#iva').val() == ""){
    $('#iva').blur().addClass('is-invalid');
    valid = false;
  }

  // validazione del cognome
  if ($('#surname').val() == ""){
    $('#surname').blur().addClass('is-invalid');
    valid = false;
  }

  // validazione email
  if (!validateEmail($('#email').val())){
    $('#email').blur().addClass('is-invalid');
    valid = false;
  }

// validazione del numero telefonico
  if (!validatePhone($('#mobile').val())){
    $('#mobile').blur().addClass('is-invalid');
    valid = false;
  }
  // validazione del CAP
  if ($('#postcode').val() == "" || $('#postcode').val().length < 5){
    $('#postcode').blur().addClass('is-invalid');
    valid = false;
  }

  // TRATTAMENTO DEI DATI PERSONALI A FINI DI SURVEY
  if (!$('#agreeSurvey1').is(":checked") && !$('#agreeSurvey2').is(":checked")){
    $('#agreeSurvey1').addClass('is-invalid');
    $('#agreeSurvey2').addClass('is-invalid');
    valid = false;
  }

  // TRATTAMENTO DEI DATI PERSONALI A FINI DI REMARKETING
  if (!$('#agreeMarketResearch1').is(":checked") && !$('#agreeMarketResearch2').is(":checked")){
    $('#agreeMarketResearch1').addClass('is-invalid');
    $('#agreeMarketResearch2').addClass('is-invalid');
    valid = false;
  }

  // CESSIONE A TERZI DEI DATI PERSONALI
  if (!$('#agreeData1').is(":checked") && !$('#agreeData2').is(":checked")){
    $('#agreeData1').addClass('is-invalid');
    $('#agreeData2').addClass('is-invalid');
    valid = false;
  }

  // TRATTAMENTO DATI PERSONALI AI FINI DELLA PROFILAZIONE
  if (!$('#agreeMarketProfiling1').is(":checked") && !$('#agreeMarketProfiling2').is(":checked")){
    $('#agreeMarketProfiling1').addClass('is-invalid');
    $('#agreeMarketProfiling2').addClass('is-invalid');
    valid = false;
  }

  return valid;
}

$(function() {
	
	
/* FORM 1 ##############################################################################################################*/
/* FORM 1 ##############################################################################################################*/
/* FORM 1 ##############################################################################################################*/
/* FORM 1 ##############################################################################################################*/
/* FORM 1 ##############################################################################################################*/

		  // submit handler del form - si esegue la validazione di campi
		  $('#contact-form').on('submit', function(e){
			
			e.preventDefault();
			
			var t = window.localStorage.getItem("activeTab");
			if(t === null){t = "#form1";}
			console.log("inviamo: "+t);
			
			// resettiamo i messaggi della richiesta
			$('.request-error').hide();
			$('.request-success').hide();
			$('.request-process').hide();
		
			 // controlliamo se il browser supporta la validazione nativa (HTML5) oppure no
			if (Modernizr.formvalidation & Modernizr.cssinvalid) {
			  // eseguamo il controllo nativo dei campi, il browser aggiunge delle pseudo classi :invalid per i campi non validi, classi supportate da Bootstrap 4
			  $('#contact-form').addClass('was-validated');
			  if ($('#contact-form')[0].checkValidity() === false){
				return false;
			  }
			} else {
			  // eseguamo come failback un check autonomo
			  console.log('not valid data submitted');
			  $(".is-invalid").removeClass("is-invalid");
			  if(!validate()){
				console.log('not valid data submitted');
				return false;
			  }
			}
		
			// disabilitiamo il bottone di submit
			$('[type=submit]').blur().prop('disabled', true);
			// mostriamo un messaggio di caricamento in corso
			$('.request-process').show();
		
		
		   
			// lanciamo la chiamata in POST al servizio di backend
			
			if(window.localStorage.getItem("formCompilato") == 1){
				  $('#formCompilato').fadeIn('slow').delay(15000).fadeOut('slow');
					return;
				  }
			/* INVIO LIBERI PROFESSIONISTI ############################################################ */
			/* INVIO LIBERI PROFESSIONISTI ############################################################ */
			/* INVIO LIBERI PROFESSIONISTI ############################################################ */
			
			
			$.post('send-request.php', $("#contact-form").serialize())
				.done(function(data) {
				data = JSON.parse(data);
				console.log(data);
				console.log("HasError:", data.responses[0].hasErrors);
				if(data.responses[0].hasErrors === false){
				  // mostriamo un messaggio di successo
				  $('.request-success').fadeIn('slow', function(){
					  
					  window.localStorage.setItem("formCompilato", 1);
					// nascondiamo l'header della sezione
					$('.section-form .section-heading').fadeOut('slow');
					// nascondiamo il form
					$('#contact-form').fadeOut('slow');
				  });
				  $(window).trigger("form-submitted");
				}else{
				  // mostriamo un messaggio di errore che verrà rimosso dopo 3 secondi
					$('.request-error').fadeIn('slow').delay(15000).fadeOut('slow');
				}
				})
			  .fail(function(err, textStatus) {
				// mostriamo un messaggio di errore che verrà rimosso dopo 3 secondi
				$('.request-error').fadeIn('slow').delay(15000).fadeOut('slow');
			  })
			  .always(function(){
					setTimeout(function(){
				  // riattiviamo il bottone di submit
				  $('[type=submit]').blur().prop('disabled', false);
				  // nascondiamo il messaggio di caricamento in corso
				  $('.request-process').fadeOut('fast');
					},200);
			  });
			  
			  
			  
			  
		  });
		  
		  
		  
		  
		  /* FORM 2 ##############################################################################################################*/
		  /* FORM 2 ##############################################################################################################*/
		  /* FORM 2 ##############################################################################################################*/
		  /* FORM 2 ##############################################################################################################*/
		  /* FORM 2 ##############################################################################################################*/
		  
		  // submit handler del form - si esegue la validazione di campi
		  $('#contact-form2').on('submit', function(e){
				
			e.preventDefault();
			
			var t = window.localStorage.getItem("activeTab");
			if(t === null){t = "#form1";}
			console.log("inviamo: "+t);
			
			// resettiamo i messaggi della richiesta
			$('.request-error').hide();
			$('.request-success').hide();
			$('.request-process').hide();
		
			 // controlliamo se il browser supporta la validazione nativa (HTML5) oppure no
			if (Modernizr.formvalidation & Modernizr.cssinvalid) {
			  // eseguamo il controllo nativo dei campi, il browser aggiunge delle pseudo classi :invalid per i campi non validi, classi supportate da Bootstrap 4
			  $('#contact-form2').addClass('was-validated');
			  if ($('#contact-form2')[0].checkValidity() === false){
				return false;
			  }
			} else {
			  // eseguamo come failback un check autonomo
			  console.log('not valid data submitted');
			  $(".is-invalid").removeClass("is-invalid");
			  if(!validate()){
				console.log('not valid data submitted');
				return false;
			  }
			}
		
			// disabilitiamo il bottone di submit
			$('[type=submit]').blur().prop('disabled', true);
			// mostriamo un messaggio di caricamento in corso
			$('.request-process').show();
		
		
		   
			// lanciamo la chiamata in POST al servizio di backend
			if(window.localStorage.getItem("formCompilato") == 2){
				   $('#formCompilato').fadeIn('slow').delay(15000).fadeOut('slow');
					return;
				  }
			  /* INVIO AZIENDE < 50 ############################################################ */
			  /* INVIO AZIENDE < 50 ############################################################ */
			  /* INVIO AZIENDE < 50 ############################################################ */
			  
			  $.post('send-request2.php', $("#contact-form2").serialize())
				.done(function(data) {
				data = JSON.parse(data);
				console.log(data);
				console.log("HasError:", data.responses[0].hasErrors);
				if(data.responses[0].hasErrors === false){
				  // mostriamo un messaggio di successo
				  $('.request-success').fadeIn('slow', function(){
					  window.localStorage.setItem("formCompilato", 2);
					// nascondiamo l'header della sezione
					$('.section-form .section-heading').fadeOut('slow');
					// nascondiamo il form
					$('#contact-form2').fadeOut('slow');
				  });
				  $(window).trigger("form-submitted");
				}else{
				  // mostriamo un messaggio di errore che verrà rimosso dopo 3 secondi
					$('.request-error').fadeIn('slow').delay(15000).fadeOut('slow');
				}
				})
			  .fail(function(err, textStatus) {
				// mostriamo un messaggio di errore che verrà rimosso dopo 3 secondi
				$('.request-error').fadeIn('slow').delay(15000).fadeOut('slow');
			  })
			  .always(function(){
					setTimeout(function(){
				  // riattiviamo il bottone di submit
				  $('[type=submit]').blur().prop('disabled', false);
				  // nascondiamo il messaggio di caricamento in corso
				  $('.request-process').fadeOut('fast');
					},200);
			  });
			  
			 
			  
		  });
		  
		  
		  
		  /* FORM 3 #####################################################################################################*/
		   /* FORM 3 #####################################################################################################*/
		    /* FORM 3 #####################################################################################################*/
			 /* FORM 3 #####################################################################################################*/
			 
			 // submit handler del form - si esegue la validazione di campi
		  $('#contact-form3').on('submit', function(e){
				
			e.preventDefault();
			
			var t = window.localStorage.getItem("activeTab");
			if(t === null){t = "#form1";}
			console.log("inviamo: "+t);
			
			// resettiamo i messaggi della richiesta
			$('.request-error').hide();
			$('.request-success').hide();
			$('.request-process').hide();
		
			 // controlliamo se il browser supporta la validazione nativa (HTML5) oppure no
			if (Modernizr.formvalidation & Modernizr.cssinvalid) {
			  // eseguamo il controllo nativo dei campi, il browser aggiunge delle pseudo classi :invalid per i campi non validi, classi supportate da Bootstrap 4
			  $('#contact-form3').addClass('was-validated');
			  if ($('#contact-form3')[0].checkValidity() === false){
				return false;
			  }
			} else {
			  // eseguamo come failback un check autonomo
			  console.log('not valid data submitted');
			  $(".is-invalid").removeClass("is-invalid");
			  if(!validate()){
				console.log('not valid data submitted');
				return false;
			  }
			}
		
			// disabilitiamo il bottone di submit
			$('[type=submit]').blur().prop('disabled', true);
			// mostriamo un messaggio di caricamento in corso
			$('.request-process').show();
		
		
		   
			// lanciamo la chiamata in POST al servizio di backend
			
			 /* INVIO AZIENDE 50 + ############################################################ */
			  /* INVIO AZIENDE 50 + ############################################################ */
			  /* INVIO AZIENDE 50 + ############################################################ */
	  
			  if(window.localStorage.getItem("formCompilato") == 3){
				  
				  $('#formCompilato').fadeIn('slow').delay(15000).fadeOut('slow');
					return;
				  }
			  
			  $.post('send-request3.php', $("#contact-form3").serialize())
				.done(function(data) {
				data = JSON.parse(data);
				console.log(data);
				console.log("HasError:", data.responses[0].hasErrors);
				if(data.responses[0].hasErrors === false){
					window.localStorage.setItem("formCompilato", 3);
				  // mostriamo un messaggio di successo
				  $('.request-success').fadeIn('slow', function(){
					// nascondiamo l'header della sezione
					$('.section-form .section-heading').fadeOut('slow');
					// nascondiamo il form
					$('#contact-form3').fadeOut('slow');
				  });
				  $(window).trigger("form-submitted");
				}else{
				  // mostriamo un messaggio di errore che verrà rimosso dopo 3 secondi
					$('.request-error').fadeIn('slow').delay(15000).fadeOut('slow');
				}
				})
			  .fail(function(err, textStatus) {
				// mostriamo un messaggio di errore che verrà rimosso dopo 3 secondi
				$('.request-error').fadeIn('slow').delay(15000).fadeOut('slow');
			  })
			  .always(function(){
					setTimeout(function(){
				  // riattiviamo il bottone di submit
				  $('[type=submit]').blur().prop('disabled', false);
				  // nascondiamo il messaggio di caricamento in corso
				  $('.request-process').fadeOut('fast');
					},200);
			  });
			  
			 
			  
		  });
});
