var offset = 200;
var duration = 500;
var formFired = false;

$(function() {
    console.log("main script");
    $('#legal-text').on('hidden.bs.collapse', function () {
        $(".btn-legal .glyphicon-menu-up").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
    });
    $('#legal-text').on('shown.bs.collapse', function () {
        $(".btn-legal .glyphicon-menu-down").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        $('html, body').animate({scrollTop: $('#legal-text').offset().top + -50}, duration);
    });

    $('.scroll-to-top').on('click', function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, duration);
        return false;
    });

    $('body.desktop .box-container').on('click', function (event) {
        console.log("desktop CTA fired");
        event.preventDefault();
        $('html, body').animate({scrollTop: $("#contact-form").offset().top }, duration);
        return false;
    });

    $('.cta-xs').on('click', function (event) {
        console.log("mobile CTA fired");
        event.preventDefault();
        $('html, body').animate({scrollTop: $("#contact-form").offset().top }, duration);
        return false;
    });

    $(window).scroll(function() {
        if ($(window).scrollTop() > offset) {
            $('.scroll-to-top').fadeIn(duration);
        } else {
            $('.scroll-to-top').fadeOut(duration);
        }
    });
});
