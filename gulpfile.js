'use strict';

const fs = require('fs');
const gutil = require('gulp-util');
const gulp = require('gulp');
const del = require('del');
const htmlmin = require('gulp-htmlmin');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const ftp = require('vinyl-ftp');
const versionAppend = require('gulp-version-append');
const bump = require('gulp-bump');
const argv = require('yargs').argv;
const imageResize = require('gulp-image-resize');
const webp = require('gulp-webp');
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const merge = require('merge-stream');
const php = require('gulp-connect-php');
const browserSync = require('browser-sync');
const Readable = require('stream').Readable;
const source = require('vinyl-source-stream');
const phpConfig = require("gulp-phpconfig");
const through = require('through2');

if (!process.env.NODE_ENV)
  process.env.NODE_ENV = "staging";

const config = require('config');

// server BrowserSync
const server = browserSync.create();
// cartella di destinazione
const dest = './dist';
// cartella da usar per il deploy
const deploy = dest + '/**/*';
// percorsi dei vari tipi di file
const paths = {
  fonts: {
    src: './src/fonts/**/*',
    dest: dest + '/fonts'
  },
  styles: {
    src: './src/css/**/*.css',
    dest: dest + '/css'
  },
  styles_async: {
    css: {
      src: './src/css-async/**/*.css'
    },
    sass: {
      src: './src/css-async/**/*.scss'
    },
    dest: dest + '/css'
  },
  scripts: {
    src: './src/js/**/*.js',
    dest: dest + '/js'
  },
  scripts_async: {
    src: './src/js-async/**/*.js',
    dest: dest + '/js'
  },
  html: {
    src: './src/html/**/*',
    dest: dest
  },
  php: {
    src: './src/php/**/*',
    dest: dest
  },
  images: {
    dir: './src/images',
    src: './src/images/**/*',
    dest: dest + '/images'
  },
  svg: {
    src: './src/svg/**/*',
    dest: dest + '/images'
  },
  favicon: {
    src: './src/favicon/**/*',
    dest: dest + '/images'
  }
};

gulp.task('reload', (done) => {
  server.reload();
  done();
});

gulp.task('serve', (done) => {
  php.server({ base: 'dist', port: 8010, keepalive: true, debug: false}, () =>
    server.init({
      logLevel: 'silent',
      proxy: '127.0.0.1:8010',
      open: true,
      notify: false,
      debug: false
    })
  );
  done();
});

gulp.task('copy-svg', () => {
  return gulp.src(paths.svg.src)
    .pipe(gulp.dest(paths.svg.dest));
});

gulp.task('copy-images', () => {
  return gulp.src(paths.images.src)
    .pipe(gulp.dest(paths.images.dest));
});

gulp.task('copy-favicon', () => {
  return gulp.src(paths.favicon.src)
    .pipe(gulp.dest(paths.favicon.dest));
});

gulp.task('images', gulp.parallel('copy-images', 'copy-svg', 'copy-favicon'));

gulp.task('copy-html', () => {
  return gulp.src(paths.html.src)
    //.pipe(versionAppend(['html', 'js', 'css', 'jpeg', 'jpg', 'png', 'webp'],{appendType: 'guid'}))
    .pipe(htmlmin({collapseWhitespace: true, minifyCSS: true, minifyJS: true}))
    .pipe(gulp.dest(paths.html.dest));
});

gulp.task('copy-php', () => {
  return gulp.src(paths.php.src)
    .pipe(gulp.dest(paths.php.dest));
});

gulp.task('config-php', (done) => {
  const readable = new Readable();
  readable._read = () => {}; // redundant? see update below
  readable.push(JSON.stringify(config.get('sfmc')));
  readable.push(null);
  readable
    .pipe(source('config.js'))
    .pipe(through.obj((file, enc, cb) => {
      file.contents = Buffer.from(JSON.stringify(config.get('sfmc'), 'utf8'));
      cb(null, file);
    }))
    .pipe(phpConfig({ filename: 'config.php', openTag: '<?php', closeTag: '', define: true }))
    .pipe(gulp.dest(dest))
    .on('end', () => done() );
});

gulp.task('pages', gulp.parallel('copy-html', 'copy-php', 'config-php'));

gulp.task('clean', () => {
  return del([dest+"/**/*"]);
});

gulp.task('script', () => {
  return gulp.src(paths.scripts.src)
   .pipe(concat('app.js'))
   .pipe(uglify())
   .pipe(gulp.dest(paths.scripts.dest));
});

gulp.task('script-async', () => {
  return gulp.src(paths.scripts_async.src)
    .pipe(concat('async.js'))
    .pipe(uglify())
    .pipe(gulp.dest(paths.scripts_async.dest));
});

gulp.task('scripts', gulp.parallel('script', 'script-async'));

gulp.task('copy-fonts', () => {
  return gulp.src(paths.fonts.src)
    .pipe(gulp.dest(paths.fonts.dest));
});

gulp.task('copy-css', () => {
  return gulp.src(paths.styles.src)
    .pipe(concat('style.css'))
    .pipe(cleanCSS())
    .pipe(gulp.dest(paths.styles.dest));
});

gulp.task('copy-css-async', () => {
  let sassStream,
      cssStream;

  //compile sass
  sassStream = gulp.src(paths.styles_async.sass.src)
    .pipe(sass({ errLogToConsole: true}))
    //.pipe(sourcemaps.init())
    .pipe(sass());
    //.pipe(sourcemaps.write(paths.styles_async.dest)) // salva dei file .map
    //.pipe(autoprefixer()); // aggiunge i prefissi dei vendor (es. -webkit-transform, -moz-transform, -ms-transform, -o-transform)


  //select additional css files
  cssStream = gulp.src(paths.styles_async.css.src);

  //merge the two streams and concatenate their contents into a single file
  return merge(sassStream, cssStream)
    .pipe(concat('async.css'))
    .pipe(cleanCSS())
    .pipe(gulp.dest(paths.styles_async.dest));
});

gulp.task('style', gulp.parallel('copy-fonts', 'copy-css', 'copy-css-async'));


gulp.task('bump', (done) => {
  if (process.env.NODE_ENV !== 'production') {
    gutil.log("bump non eseguito");
    return done();
  }
  /// <summary>
  /// It bumps revisions
  /// Usage:
  /// 1. gulp bump : bumps the package.json and bower.json to the next minor revision.
  ///   i.e. from 0.1.1 to 0.1.2
  /// 2. gulp bump --ver 1.1.1 : bumps/sets the package.json and bower.json to the
  ///    specified revision.
  /// 3. gulp bump --type major       : bumps 1.0.0
  ///    gulp bump --type minor       : bumps 0.1.0
  ///    gulp bump --type patch       : bumps 0.0.2
  ///    gulp bump --type prerelease  : bumps 0.0.1-2
  /// </summary>

  let type = argv.type;
  let version = argv.ver;
  let options = {};
  if (version) {
    options.version = version;
  } else {
    options.type = type;
  }

  return gulp.src(['./package.json'])
    .pipe(bump(options))
    .pipe(gulp.dest('./'));
});

gulp.task('ftp', () => {
  let options = config.get('ftp');
  if (!options.log)
    options.log = gutil.log;
  let conn = ftp.create(options);

  return gulp.src( deploy, { buffer: false } )
    //.pipe( conn.newer( ftpconf[env].dest ) ) // only upload newer files
    //.pipe( conn.newerOrDifferentSize( ftpconf[env].dest ) ) // only upload newer or different size files
    .pipe( conn.dest(config.get('ftp.dest')) );
});

gulp.task('build', gulp.series('clean', gulp.parallel('images', 'pages', 'scripts', 'style')));

gulp.task('deploy:staging', done => {
  return gutil.log('deprecated: use the command `npm run deploy` instead (or directly `gulp deploy`)');
  done();
});
gulp.task('deploy:production', done => {
  return gutil.log('deprecated: use the command `npm run deploy-production` instead (or directly `NODE_ENV=production gulp deploy`)');
  done();
});
gulp.task('deploy', gulp.series('bump', 'build', 'ftp'));

gulp.task('watching-tasks', () =>  {
  gulp.watch(paths.html.src, gulp.series('copy-html', 'reload'));
  gulp.watch(paths.php.src, gulp.series('copy-php', 'reload'));
  gulp.watch(paths.styles.src, gulp.series('style', 'reload'));
  gulp.watch(paths.styles_async.css.src, gulp.series('style', 'reload'));
  gulp.watch(paths.styles_async.sass.src, gulp.series('style', 'reload'));
  gulp.watch(paths.scripts.src, gulp.series('scripts', 'reload'));
  gulp.watch(paths.scripts_async.src, gulp.series('scripts', 'reload'));
  gulp.watch(paths.svg.src, gulp.series('copy-svg', 'reload'));
  gulp.watch(paths.images.src, gulp.series('copy-images', 'reload'));
  gulp.watch(paths.favicon.src, gulp.series('copy-favicon', 'reload'));
});

gulp.task('watch', gulp.series('build', 'serve', 'watching-tasks'));

// GESTIONE DELLE IMMAGINI DA ELABORARE - PRE-PRODUZIONE

const images = {
  'hero-mobile': {
    'src': './images-to-be-processed/hero-mobile/*',
    'sizes': [
      {
        'size': 'small',
        'width' : 750,
        'height' : 750,
        'quality': 0.80
      },
      {
        'size': 'extra-small',
        'width' : 375,
        'height' : 375,
        'quality': 0.80
      }
    ]
  },
  'hero-desktop': {
    'src': './images-to-be-processed/hero-desktop/*',
    'sizes': [
      {
        'size': 'extra-large',
        'width' : 1920,
        'height' : 1920,
        'quality': 0.90
      },
      {
        'size': 'large',
        'width' : 1300,
        'height' : 1300,
        'quality': 0.80
      }
    ]
  },
  'punti-prodotto': {
    'src': './images-to-be-processed/punti-prodotto/*',
    'sizes': [
      {
        'size': 'large',
        'width' : 570,
        'height' : 440,
        'quality': 0.75
      },
      {
        'size': 'medium',
        'width' : 350,
        'height' : 272,
        'quality': 0.75
      },
      {
        'size': 'small',
        'width' : 290,
        'height' : 272,
        'quality': 0.60
      }
    ]
  },
  'gallery-thumb': {
    'src': './images-to-be-processed/gallery-thumb/*',
    'sizes': [
      {
        'size': 'large',
        'width' : 768,
        'height' : 432,
        'quality': 0.75
      },
      {
        'size': 'medium',
        'width' : 480,
        'height' : 270,
        'quality': 0.75
      },
      {
        'size': 'small',
        'width' : 375,
        'height' : 211,
        'quality': 0.60
      },
      {
        'size': 'extra-small',
        'width' : 360,
        'height' : 203,
        'quality': 0.60
      }
    ]
  },
  'gallery': {
    'src': './images-to-be-processed/gallery/*',
    'sizes': [
      {
        'size': 'extra-large',
        'width' : 1920,
        'height' : 1080,
        'quality': 0.75
      },
      {
        'size': 'large',
        'width' : 1440,
        'height' : 810,
        'quality': 0.75
      },
      {
        'size': 'medium',
        'width' : 768,
        'height' : 768,
        'quality': 0.75
      },
      {
        'size': 'small',
        'width' : 375,
        'height' : 375,
        'quality': 0.60
      },
      {
        'size': 'extra-small',
        'width' : 360,
        'height' : 360,
        'quality': 0.60
      }
    ]
  }
};

gulp.task('towebp', () => {
    gutil.log('==> transform to webp');
    return gulp.src(paths.images.dir + '/**/*.{png,jpeg,jpg}')
      .pipe(webp())
      .pipe(gulp.dest(paths.images.dir));
});

function _resize(type, params) {
  gutil.log('==> resize size *** ' + params.size + ' ***');
  gutil.log('source', images[type].src);
  gutil.log('dest', paths.images.dir + '/' +  params.size);
  return gulp.src(images[type].src)
    .pipe(imageResize({
      width : params.width,
      height : params.height,
      quality : params.quality,
      crop : true,
      upscale : false
    }))
    .pipe( gulp.dest(paths.images.dir + '/' +  params.size) );
}

function processFormat(format, done) {
  let processed = 0;
  images[format].sizes.forEach( (element, index) => {
    _resize(format, element);
    processed++;
    if (processed === images[format].sizes.length){
      gutil.log('done');
      done();
    }
  });
}

// Punti Prodotto
gulp.task('puntiProdotto', (done) => {
  processFormat('punti-prodotto', ()=>{
    done();
  });
});

// Hero image "mobile"
gulp.task('heroMobile', (done) => {
  processFormat('hero-mobile', ()=>{
    done();
  });
});

// Hero image "desktop"
gulp.task('heroDesktop', (done) => {
  processFormat('hero-desktop', ()=>{
    done();
  });
});

// Gallery Thumb
gulp.task('galleryThumb', (done) => {
  processFormat('gallery-thumb', ()=>{
    done();
  });
});

// Gallery Slide
gulp.task('gallerySlide', (done) => {
  processFormat('gallery', ()=>{
    done();
  });
});

// Task default (watch)
gulp.task('default', gulp.parallel('watch'));